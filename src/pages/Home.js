import React, { Component } from 'react'
import axios from 'axios';

import { Link } from 'react-router-dom'



import '../components/Styles.css';

import Welcome from '../components/Welcome';


export default class Home extends Component {
    state = {
        name: 'HELLO!',
        data: [],

    }


    async componentDidMount() {
        const response = await axios.get('https://shop-backendapi.herokuapp.com/api/shop');
        const json = (response.data.data);

        this.setState({ data: json });

        this.setState({
            name: 'WELCOME!'
        })
    }

    render() {

        return (
            <div className="container my-5">

                <Welcome myname={this.state.name} />

                <ul>

                    <div class="card-deck">
                        {this.state.data.map(el => (
                            <div class="card-group ">

                                <div className="card border-primary mb-5" style={{ maxWidth: '18rem' }}>
                                    <div class="card-body">
                                        <img src={el.photo} className="card-img-top styles" alt="..." />
                                        <div className="card-body center" >
                                            <h5 className="card-text">{el.name}</h5><br />


                                            <Link to={`/menu/${el.id}`} className="btn btn-outline-success" >Menu</Link>
                                            &nbsp;
                                            <a href={`https://www.google.com/search?q=${el.location.lat}+${el.location.lgn}`} className="btn btn-outline-primary">Location</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}

                    </div>

                </ul>
            </div>



        )
    }



}
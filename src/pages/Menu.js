import React, { Component } from 'react'
import axios from 'axios';

import '../components/Styles.css';
// import f1 from './img/f1.jpg';


export default class Menu extends Component {
    state = {
        menu: [],
        data: {}
    }

    async componentDidMount() {
        const id = this.props.match.params.id
        const res = await axios.get(`https://shop-backendapi.herokuapp.com/api/shop/${id}`);
        console.log(id)
        const json = (res.data.data.menus);
        const jsons = (json.data);

        console.log(json)

        this.setState({ menu: json, data: jsons });


    }

    render() {
        return (
            <div classname="container">
                <div classname="card-deck">
                    {this.state.menu.map(e => (
                        <div classname="card">
                            {/* <img src="src={f1}" className="Menu-f1" alt="f1" /> */}
                            {/* classname="card-img-top" alt="..."  */}
                            <div classname="card-body">
                            <ul class="list-group">
                            <li class="list-group-item list-group-item-primary font">{e.name}</li>
                            
                                <p class="list-group-item list-group-item-action">PRICE : {e.price.$numberDecimal}</p>
                                </ul>
                            </div>
                            <div classname="card-footer">

                            </div>
                        </div>
                    ))}
                    <small style={{textAlign: "center"}} classname="text-muted">Thank You!</small>
                </div>
            </div>



        )
    }
}